import axios from 'axios'
// create an axios instance
export const service = axios.create({
  baseUrl: 'hhttps://api.dev.cdgfossil.com'
});

const baseUrl = 'https://api.dev.cdgfossil.com'; //1
const backendUrl = 'https://44276b09.ngrok.io'; //0

const ignoreToken = [
  '/v2/rpc/auth/login',
  '/v2/rpc/auth/register',
  '/v2/rpc/auth/token/refresh'
]
service.interceptors.response.use(
  response => response,
  error => {
    //window.alert('error')
    switch (error.response.status) {
      case 400:
        break
      case 401:
        let token = localStorage.getItem('refreshToken');
        if (token===null){
          //service.redirectTo(document, '/login');
        } else {
          service.api.post(1,'/v2/rpc/auth/token/refresh', {refreshToken: token})
            .then(response => {
              console.log(response);
              //window.alert("Accesss Token Expired");
              localStorage.setItem('authUser', response.data.accessToken);
              localStorage.setItem('refreshToken', response.data.refreshToken);
            });
        }
        break
      case 404:
        service.redirectTo(document, '/404')
        break
      case 409:
        break
      default:
        //service.redirectTo(document, '/500')
        break
    }
    return Promise.reject(error)
  })
service.redirectTo = (document, path) => {
  document.location = path
}
service.api = {}

service.api.get = (type, path, callback) => {
  let token = localStorage.getItem('authUser');
  let config = {
    headers: {
      Authorization: 'Bearer ' + token,
      'X-Authenticated-Uid': localStorage.getItem('uid')
    }
  };
  if (type == 1) return service.get(baseUrl + path, config);
  return service.get(backendUrl + path, config);
}

service.api.post = (type, path, params, config, callback) => {
  console.log(type + " " + path + " " + JSON.stringify(params) + " " + baseUrl)
  let token = localStorage.getItem('authUser');
  if (!config) {
    config = {};
  }

  if (!config.headers) {
    config.headers = {

    };
  }
  
  if (ignoreToken.indexOf(path) == -1 ) {
    config.headers = {
      Authorization : 'Bearer ' + token,
      'x-authenticated-uid': localStorage.getItem('uid'),
    }
  }

  if (ignoreToken.indexOf(path) == 1) {
    config.headers = {
      'locale' : 'en'
    }
  }

  if (type === 1) return service.post(baseUrl + path, params, config);
  else return service.post(backendUrl + path, params, config);
}
